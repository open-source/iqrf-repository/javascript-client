/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Metadata profile HWPID version range - raw API response
 * @internal
 */
export interface HwpidVersionsRaw {

	/**
	 * Minimum HWPID version
	 */
	min: number;

	/**
	 * Maximum HWPID version
	 */
	max: number;
}

/**
 * Accumulator power metadata - raw API response
 * @internal
 */
export interface AccumulatorPowerRaw {

	/**
	 * Device is powered by accumulator
	 */
	present: boolean;

	/**
	 * Accumulator type
	 */
	type: string | null;

	/**
	 * Low charge level
	 */
	lowLevel: number | null;
}

/**
 * Battery power metadata - raw API response
 * @internal
 */
export interface BatterPowerRaw {

	/**
	 * Device is powered by battery
	 */
	present: boolean;

	/**
	 * Battery type
	 */
	type: string | null;

	/**
	 * Battery change threshold
	 */
	changeThreshold: number | null;
}

/**
 * Power supply metadata - raw API response
 * @internal
 */
export interface PowerSupplyRaw {

	/**
	 * Mains powered
	 */
	mains: boolean;

	/**
	 * Accumulator
	 */
	accumulator: AccumulatorPowerRaw;

	/**
	 * Battery
	 */
	battery: BatterPowerRaw;

	/**
	 * Minimum voltage
	 */
	minVoltage: number;
}


/**
 * Metadata profile - raw API response
 * @internal
 */
export interface MetadataProfileRaw {

	/**
	 * HWPID version range
	 */
	hwpidVersions: HwpidVersionsRaw;

	/**
	 * Routes packets
	 */
	routing: boolean;

	/**
	 * Operates in beaming mode
	 */
	beaming: boolean;

	/**
	 * Repeater device
	 */
	repeater: boolean;

	/**
	 * Aggregates FRC data
	 */
	frcAggregation: boolean;

	/**
	 * Compatible with the IQAROS system
	 */
	iqarosCompatible: boolean;

	/**
	 * Implemented sensors
	 */
	iqrfSensor: number[];

	/**
	 * Implemented binary outputs
	 */
	iqrfBinaryOutput: number;

	/**
	 * Power supply metadata
	 */
	powerSupply: PowerSupplyRaw;
}

/**
 * Static metadata - raw API response
 * @internal
 */
export interface StaticMetadataRaw {

	/**
	 * Metadata version
	 */
	version: number;

	/**
	 * Product HWPID profiles
	 */
	profiles: MetadataProfileRaw[];
}

/**
 * Product metadata - raw API response
 * @internal
 */
export interface ProductMetadataRaw {

	/**
	 * Static metadata (exist for every product)
	 */
	static: StaticMetadataRaw[];

	/**
	 * Extra metadata (dynamic)
	 */
	extra: unknown;
}

/**
 * HWPID version range
 */
export class HwpidVersions {

	/**
	 * @property {number} min HWPID version floor
	 */
	public readonly min: number;

	/**
	 * @property {number} max HWPID version ceiling
	 */
	public readonly max: number;

	/**
	 * Constructs manufacturer
	 * @param {number} min HWPID version floor
	 * @param {number} max HWPID version ceiling
	 */
	public constructor(min: number, max: number) {
		this.min = min;
		this.max = max;
	}

	/**
	 * Deserializes HWPID version range from raw API response
	 * @param {HwpidVersionsRaw} data HWPID version data (raw API response)
	 * @return {HwpidVersions} Deserialized HWPID versions data
	 */
	public static deserialize(data: HwpidVersionsRaw): HwpidVersions {
		return new HwpidVersions(data.min, data.max);
	}

}

/**
 * Accumulator metadata
 */
export class AccumulatorPower {

	/**
	 * @property {boolean} present Accumulator powered
	 */
	public readonly present: boolean;

	/**
	 * @property {string | null} type Accumulator type
	 */
	public readonly type: string | null;

	/**
	 * @property {number | null} lowLevel Low charge level
	 */
	public readonly lowLevel: number | null;

	/**
	 * Constructs accumulator metadata
	 * @param {boolean} present Accumulator powered
	 * @param {string | null} type Accumulator type
	 * @param {number | null} lowLevel Low charge level
	 */
	public constructor(present: boolean, type: string | null, lowLevel: number | null) {
		this.present = present;
		this.type = type;
		this.lowLevel = lowLevel;
	}

	/**
	 * Deserializes accumulator power from raw API response
	 * @param {AccumulatorPowerRaw} data Accumulator power data (raw API response)
	 * @return {AccumulatorPower} Deserialized accumulator power data
	 */
	public static deserialize(data: AccumulatorPowerRaw): AccumulatorPower {
		return new AccumulatorPower(data.present, data.type, data.lowLevel);
	}

}

/**
 * Battery metadata
 */
export class BatteryPower {

	/**
	 * @property {boolean} present Battery powered
	 */
	public readonly present: boolean;

	/**
	 * @property {string | null} type Battery type
	 */
	public readonly type: string | null;

	/**
	 * @property {number | null} changeThreshold Battery change threshold
	 */
	public readonly changeThreshold: number | null;

	/**
	 * Constructs battery metadata
	 * @param {boolean} present Battery powered
	 * @param {string | null} type Battery type
	 * @param {number | null} changeThreshold Battery change threshold
	 */
	public constructor(present: boolean, type: string | null, changeThreshold: number | null) {
		this.present = present;
		this.type = type;
		this.changeThreshold = changeThreshold;
	}

	/**
	 * Deserializes battery power from raw API response
	 * @param {BatterPowerRaw} data Battery power data (raw API response)
	 * @return {BatteryPower} Deserialized battery power data
	 */
	public static deserialize(data: BatterPowerRaw): BatteryPower {
		return new BatteryPower(data.present, data.type, data.changeThreshold);
	}

}

/**
 * Power supply metadata
 */
export class PowerSupply {

	/**
	 * @property {boolean} mains Mains powered
	 */
	public readonly mains: boolean;

	/**
	 * @property {AccumulatorPower} accumulator Accumulator metadata
	 */
	public readonly accumulator: AccumulatorPower;

	/**
	 * @property {BatteryPower} battery Battery metadata
	 */
	public readonly battery: BatteryPower;

	/**
	 * @property {number} minVoltage Minimum voltage level
	 */
	public readonly minVoltage: number;

	/**
	 * Constructs power supply metadata
	 * @param {boolean} mains Mains powered
	 * @param {AccumulatorPower} accumulator Accumulator metadata
	 * @param {BatteryPower} battery Battery metadata
	 * @param {number} minVoltage Minimum voltage level
	 */
	public constructor(mains: boolean, accumulator: AccumulatorPower, battery: BatteryPower, minVoltage: number) {
		this.mains = mains;
		this.accumulator = accumulator;
		this.battery = battery;
		this.minVoltage = minVoltage;
	}

	/**
	 * Deserializes power supply metadata from raw API response
	 * @param {PowerSupplyRaw} data Power supply data (raw API response)
	 * @return {PowerSupply} Deserialized power supply data
	 */
	public static deserialize(data: PowerSupplyRaw): PowerSupply {
		return new PowerSupply(
			data.mains,
			AccumulatorPower.deserialize(data.accumulator),
			BatteryPower.deserialize(data.battery),
			data.minVoltage,
		);
	}

}

/**
 * Metadata profile
 */
export class MetadataProfile {

	/**
	 * @property {HwpidVersions} hwpidVersions HWPID version range
	 */
	public readonly hwpidVersions: HwpidVersions;

	/**
	 * @property {boolean} routing Routes packets
	 */
	public readonly routing: boolean;

	/**
	 * @property {boolean} beaming Operates in beaming mode
	 */
	public readonly beaming: boolean;

	/**
	 * @property {boolean} repeater Repeater device
	 */
	public readonly repeater: boolean;

	/**
	 * @property {boolean} frcAggregation Aggregates FRC data
	 */
	public readonly frcAggregation: boolean;

	/**
	 * @property {boolean} iqarosCompatible Compatible with the IQAROS system
	 */
	public readonly iqarosCompatible: boolean;

	/**
	 * @property {number[]} iqrfSensor Implemented sensors
	 */
	public readonly iqrfSensor: number[];

	/**
	 * @property {number} iqrfBinaryOutput Implemented binary outputs
	 */
	public readonly iqrfBinaryOutput: number;

	/**
	 * @property {PowerSupply} powerSupply Power supply metadata
	 */
	public readonly powerSupply: PowerSupply;

	/**
	 * Constructs metadata profile
	 * @param {HwpidVersions} hwpidVersion HWPID version range
	 * @param {boolean} routing Routes packets
	 * @param {boolean} beaming Operates in beaming mode
	 * @param {boolean} repeater Repeater device
	 * @param {boolean} frcAggregation Aggregates FRC data
	 * @param {boolean} iqarosCompatible Compatible with the IQAROS system
	 * @param {number[]} iqrfSensor Implemented sensors
	 * @param {number} iqrfBinaryOutput Implemented binary outputs
	 * @param {PowerSupply} powerSupply Power supply metadata
	 */
	public constructor(hwpidVersion: HwpidVersions, routing: boolean, beaming: boolean, repeater: boolean, frcAggregation: boolean, iqarosCompatible: boolean, iqrfSensor: number[], iqrfBinaryOutput: number, powerSupply: PowerSupply) {
		this.hwpidVersions = hwpidVersion;
		this.routing = routing;
		this.beaming = beaming;
		this.repeater = repeater;
		this.frcAggregation = frcAggregation;
		this.iqarosCompatible = iqarosCompatible;
		this.iqrfSensor = iqrfSensor;
		this.iqrfBinaryOutput = iqrfBinaryOutput;
		this.powerSupply = powerSupply;
	}

	/**
	 * Deserializes metadata profile from raw API response
	 * @param {MetadataProfileRaw} data Metadata profile data (raw API response)
	 * @return {MetadataProfile} Deserialized metadata profile
	 */
	public static deserialize(data: MetadataProfileRaw): MetadataProfile {
		return new MetadataProfile(
			HwpidVersions.deserialize(data.hwpidVersions),
			data.routing,
			data.beaming,
			data.repeater,
			data.frcAggregation,
			data.iqarosCompatible,
			data.iqrfSensor,
			data.iqrfBinaryOutput,
			PowerSupply.deserialize(data.powerSupply),
		);
	}

}

/**
 * Static metadata
 */
export class StaticMetadata {

	/**
	 * @property {number} version Metadata version
	 */
	public readonly version: number;

	/**
	 * @property {MetadataProfile[]} profiles Product HWPID profiles
	 */
	public readonly profiles: MetadataProfile[];

	/**
	 * Constructs static metadata
	 * @param {number} version Metadata version
	 * @param {MetadataProfile[]} profiles Product HWPID profiles
	 */
	public constructor(version: number, profiles: MetadataProfile[]) {
		this.version = version;
		this.profiles = profiles;
	}

	/**
	 * Deserializes static metadata from raw API response
	 * @param {StaticMetadataRaw} data Static metadata (raw API response)
	 * @return {StaticMetadata} Deserialized static metadata
	 */
	public static deserialize(data: StaticMetadataRaw): StaticMetadata {
		return new StaticMetadata(
			data.version,
			data.profiles.map((item: MetadataProfileRaw) => MetadataProfile.deserialize(item)),
		);
	}

}

/**
 * Product metadata
 */
export class ProductMetadata {

	/**
	 * @property {StaticMetadata[]} static Static metadata (exist for every product)
	 */
	public readonly static: StaticMetadata[];

	/**
	 * @property {unknown} extra Extra metadata (dynamic)
	 */
	public readonly extra: unknown;

	/**
	 * Constructs product metadata
	 * @param {StaticMetadata[]} staticMetadata Static metadata (exist for every product)
	 * @param {unknown} extra Extra metadata (dynamic)
	 */
	public constructor(staticMetadata: StaticMetadata[], extra: unknown) {
		this.static = staticMetadata;
		this.extra = extra;
	}

	/**
	 * Return metadata profile for HWPID version
	 * @param {number | null} hwpidVersion HWPID version
	 * @param {number | null} metadataVersion Metadata version
	 * @return {MetadataProfile | null} HWPID metadata profile
	 */
	public getProfile(hwpidVersion: number | null = null, metadataVersion: number | null = null): MetadataProfile | null {
		if (this.static.length === 0) {
			return null;
		}
		let staticMetadata: StaticMetadata | undefined;
		if (metadataVersion === null) {
			// unspecified, find highest version
			staticMetadata = this.static.reduce((previous: StaticMetadata, current: StaticMetadata): StaticMetadata => {
				return (previous.version > current.version) ? previous : current;
			});
		} else {
			// find specific version
			staticMetadata = this.static.find((item: StaticMetadata) => item.version === metadataVersion);
			if (staticMetadata === undefined) {
				return null;
			}
		}
		if (staticMetadata.profiles.length === 0) {
			return null;
		}
		if (hwpidVersion === null) {
			return staticMetadata.profiles.reduce((previous: MetadataProfile, current: MetadataProfile): MetadataProfile => {
				if (previous.hwpidVersions.max === -1) {
					return previous;
				}
				if (current.hwpidVersions.max === -1) {
					return current;
				}
				return previous.hwpidVersions.max > current.hwpidVersions.max ? previous : current;
			});
		}
		const profile = staticMetadata.profiles.find((item: MetadataProfile) => {
			if (item.hwpidVersions.min === -1 && item.hwpidVersions.max >= hwpidVersion) {
				return true;
			}
			if (item.hwpidVersions.max === -1 && item.hwpidVersions.min <= hwpidVersion) {
				return true;
			}
			return item.hwpidVersions.min <= hwpidVersion && hwpidVersion <= item.hwpidVersions.max;
		});
		return profile ?? null;
	}

	/**
	 * Deserializes product metadata from raw API response
	 * @param {ProductMetadataRaw} data Product metadata (raw API response)
	 * @return {ProductMetadata} Deserialized product metadata
	 */
	public static deserialize(data: ProductMetadataRaw): ProductMetadata {
		return new ProductMetadata(
			data.static.map((item: StaticMetadataRaw): StaticMetadata => StaticMetadata.deserialize(item)),
			data.extra,
		);
	}

}
