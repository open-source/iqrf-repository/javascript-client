/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { OsDpaAttributes, type OsDpaRaw, type TrFamily } from './OsDpa';

/**
 * IQRF OS version - raw response from the API
 */
export interface OsRaw {

	/**
	 * @property {string} build IQRF OS build
	 */
	build: string;

	/**
	 * @property {string} version IQRF OS version
	 */
	version: string;

	/**
	 * @property {string} trFamily TR family
	 */
	trFamily: string;

	/**
	 * @property {number} attributes IQRF OS version attributes
	 */
	attributes: number;

	/**
	 * @property {string} downloadPath Base path to download IQRF OS files
	 */
	downloadPath: string;

}

/**
 * IQRF OS version
 */
export class Os {

	/**
	 * @property {string} build IQRF OS build
	 */
	public readonly build: string;

	/**
	 * @property {string} version IQRF OS version
	 */
	public readonly version: string;

	/**
	 * @property {TrFamily} trFamily TR family
	 */
	public readonly trFamily: TrFamily;

	/**
	 * @property {OsDpaAttributes} attributes IQRF OS version attributes
	 */
	public readonly attributes: OsDpaAttributes;

	/**
	 * @property {string} downloadBaseUrl Base URL for downloading IQRF OS files
	 */
	public readonly downloadBaseUrl: string;

	/**
	 * Constructor
	 * @param {string} build IQRF OS build
	 * @param {string} version IQRF OS version
	 * @param {TrFamily} trFamily TR family
	 * @param {OsDpaAttributes} attributes IQRF OS version attributes
	 * @param {string} downloadBaseUrl Base URL for downloading IQRF OS files
	 */
	public constructor(
		build: string,
		version: string,
		trFamily: TrFamily,
		attributes: OsDpaAttributes,
		downloadBaseUrl: string,
	) {
		this.build = build;
		this.version = version;
		this.trFamily = trFamily;
		this.attributes = attributes;
		this.downloadBaseUrl = downloadBaseUrl;
	}

	/**
	 * Deserialize IQRF OS version from raw response from the API
	 * @param {OsRaw} raw Raw response from the API
	 * @return {Os} Deserialized OS
	 */
	public static deserialize(raw: OsRaw): Os {
		return new Os(
			raw.build,
			raw.version,
			raw.trFamily as TrFamily,
			OsDpaAttributes.deserialize(raw.attributes),
			raw.downloadPath,
		);
	}

	/**
	 * Deserialize IQRF OS version from raw OS&DPA response from the API
	 * @param {OsDpaRaw} raw Raw OS&DPA response from the API
	 * @return {Os} Deserialized OS
	 */
	public static deserializeOsDpa(raw: OsDpaRaw): Os {
		return new Os(
			raw.os,
			raw.osVersion,
			raw.osTrFamily as TrFamily,
			OsDpaAttributes.deserialize(raw.osAttributes),
			raw.osDownloadPath,
		);
	}

}
