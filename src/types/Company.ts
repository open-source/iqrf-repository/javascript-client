/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Company - raw response from the API
 * @internal
 */
export interface CompanyRaw {

	/**
	 * Company ID
	 */
	companyID: number;

	/**
	 * Company name
	 */
	name: string;

	/**
	 * Company homepage
	 */
	homePage: string;

}

/**
 * Company
 */
export class Company {

	/**
	 * @property {number} id Company ID
	 */
	public readonly id: number;

	/**
	 * @property {string} name Company name
	 */
	public readonly name: string;

	/**
	 * @property {string} homePage Company homepage
	 */
	public readonly homePage: string;

	/**
	 * Constructs company
	 * @param {number} id Company ID
	 * @param {string} name Company name
	 * @param {string} homePage Company homepage
	 */
	public constructor(id: number, name: string, homePage: string) {
		this.id = id;
		this.name = name;
		this.homePage = homePage;
	}

	/**
	 * Deserializes company from the API response
	 * @param {CompanyRaw} data Company data (raw response from the API)
	 * @return {Company} Deserialized company
	 */
	public static deserialize(data: CompanyRaw): Company {
		return new Company(data.companyID, data.name, data.homePage);
	}

}
