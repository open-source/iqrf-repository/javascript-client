/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { versionBcdToString } from '../utils';

import { OsDpaAttributes, type OsDpaRaw } from './OsDpa';

/**
 * DPA version - raw response from the API
 */
export interface DpaRaw {

	/**
	 * @property {string} version DPA version
	 */
	version: string;

	/**
	 * @property {number} attributes DPA version attributes
	 */
	attributes: number;

	/**
	 * @property {string} downloadPath Base path to download DPA files
	 */
	downloadPath: string;
}

/**
 * DPA version
 */
export class Dpa {

	/**
	 * @property {string} version DPA version
	 */
	public readonly version: string;

	/**
	 * @property {OsDpaAttributes} attributes DPA version attributes
	 */
	public readonly attributes: OsDpaAttributes;

	/**
	 * @property {string} downloadBaseUrl Base URL for downloading DPA files
	 */
	public readonly downloadBaseUrl: string;

	/**
	 * Constructs DPA version
	 * @param {string} version DPA version
	 * @param {OsDpaAttributes} attributes DPA version attributes
	 * @param {string} downloadBaseUrl Base URL for downloading DPA files
	 */
	public constructor(version: string, attributes: OsDpaAttributes, downloadBaseUrl: string) {
		this.version = version;
		this.attributes = attributes;
		this.downloadBaseUrl = downloadBaseUrl;
	}

	/**
	 * Serializes DPA version from raw response from the API
	 * @param {DpaRaw} raw Raw response from the API
	 * @return {Dpa} Deserialized DPA data
	 */
	public static deserialize(raw: DpaRaw): Dpa {
		return new Dpa(Dpa.deserializeVersion(raw.version), OsDpaAttributes.deserialize(raw.attributes), raw.downloadPath);
	}

	/**
	 * Deserializes DPA version from raw OS&DPA response from the API
	 * @param {OsDpaRaw} raw Raw OS&DPA response from the API
	 * @return {Dpa} Deserialized DPA data
	 */
	public static deserializeOsDpa(raw: OsDpaRaw): Dpa {
		return new Dpa(Dpa.deserializeVersion(raw.dpa), OsDpaAttributes.deserialize(raw.dpaAttributes), raw.dpaDownloadPath);
	}

	/**
	 * Deserializes DPA version from the API request
	 * @param {string} version Raw DPA version
	 * @return {string} DPA version
	 * @private
	 */
	private static deserializeVersion(version: string): string {
		const versionInt: number = Number.parseInt(version.replace(/^0/, ''), 16);
		return versionBcdToString(versionInt);
	}

}
