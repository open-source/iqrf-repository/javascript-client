/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import JsonBigInt from 'json-bigint';
import { DateTime } from 'luxon';

/**
 * Transforms raw response content into JSON with support for BigInt
 * @param {unknown} data Raw response
 * @return {object | unknown} JSON object with BigInt support
 */
export function transformJsonBigInt(data: unknown) {
	if (typeof data !== 'string') {
		return data;
	}
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-return
		return JsonBigInt({ useNativeBigInt: true }).parse(data);
	} catch {
		return data;
	}
}

/**
 * Status - raw response from the API
 * @internal
 */
export interface StatusRaw {

	/**
	 * API version
	 */
	apiVersion: number;

	/**
	 * Server host name
	 */
	hostname: string;

	/**
	 * Authenticated user name
	 */
	user: string;

	/**
	 * Server build timestamp
	 */
	buildDateTime: string;

	/**
	 * Server startup timestamp
	 */
	startDateTime: string;

	/**
	 * Current timestamp
	 */
	dateTime: string;

	/**
	 * Checksum of database tables
	 */
	databaseChecksum: bigint|string;

	/**
	 * Last database update timestamp
	 */
	databaseChangeDateTime: string;
}

/**
 * Status
 */
export class Status {

	/**
	 * API version
	 */
	public readonly apiVersion: number;

	/**
	 * Server host name
	 */
	public readonly hostname: string;

	/**
	 * Authenticated user name
	 */
	public readonly user: string;

	/**
	 * Server build date and time
	 */
	public readonly buildDateTime: DateTime;

	/**
	 * Server startup date and time
	 */
	public readonly startDateTime: DateTime;

	/**
	 * Current date and time
	 */
	public readonly dateTime: DateTime;

	/**
	 * Checksum of database tables
	 */
	public readonly databaseChecksum: bigint;

	/**
	 * Last database update date and time
	 */
	public readonly databaseChangeDateTime: DateTime;

	/**
	 * Constructor
	 * @param {number} apiVersion API version
	 * @param {string} hostname Server host name
	 * @param {string} user Authenticated user name
	 * @param {DateTime} buildDateTime Server build date and time
	 * @param {DateTime} startDateTime Server startup date and time
	 * @param {DateTime} dateTime Current date and time
	 * @param {bigint|string} databaseChecksum Checksum of database tables
	 * @param {DateTime} databaseChangeDateTime Last database update date and time
	 */
	public constructor(apiVersion: number, hostname: string, user: string, buildDateTime: DateTime, startDateTime: DateTime, dateTime: DateTime, databaseChecksum: bigint|string, databaseChangeDateTime: DateTime) {
		this.apiVersion = apiVersion;
		this.hostname = hostname;
		this.user = user;
		this.buildDateTime = buildDateTime;
		this.startDateTime = startDateTime;
		this.dateTime = dateTime;
		this.databaseChecksum = BigInt(databaseChecksum);
		this.databaseChangeDateTime = databaseChangeDateTime;
	}

	/**
	 * Deserializes status from raw API response
	 * @param {StatusRaw} raw Raw response from API
	 * @return {Status} Deserialized status
	 */
	public static deserialize(raw: StatusRaw): Status {
		return new Status(
			raw.apiVersion,
			raw.hostname,
			raw.user,
			DateTime.fromISO(raw.buildDateTime),
			DateTime.fromISO(raw.startDateTime),
			DateTime.fromISO(raw.dateTime),
			BigInt(raw.databaseChecksum),
			DateTime.fromISO(raw.databaseChangeDateTime),
		);
	}
}
