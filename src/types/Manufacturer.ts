/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Manufacturer - raw response from the API
 * @internal
 */
export interface ManufacturerRaw {

	/**
	 * Manufacturer ID
	 */
	manufacturerID: number;

	/**
	 * Manufacturer name
	 */
	name: string;

	/**
	 * Company ID
	 */
	companyID: number;

}

/**
 * Manufacturer
 */
export class Manufacturer {

	/**
	 * @property {number} id Manufacturer ID
	 */
	public readonly id: number;

	/**
	 * @property {string} name Manufacturer name
	 */
	public readonly name: string;

	/**
	 * @property {number} companyId Company ID
	 */
	public readonly companyId: number;

	/**
	 * Constructs manufacturer
	 * @param {number} id Manufacturer ID
	 * @param {string} name Manufacturer name
	 * @param {number} companyId Company ID
	 */
	public constructor(id: number, name: string, companyId: number) {
		this.id = id;
		this.name = name;
		this.companyId = companyId;
	}

	/**
	 * Deserializes manufacturer from the API response
	 * @param {ManufacturerRaw} data Manufacturer data (raw response from the API)
	 * @return {Manufacturer} Deserialized manufacturer
	 */
	public static deserialize(data: ManufacturerRaw): Manufacturer {
		return new Manufacturer(data.manufacturerID, data.name, data.companyID);
	}

}
