/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ProductMetadata, type ProductMetadataRaw } from './Metadata';

/**
 * RF modes
 */
export class RfModes {

	/**
	 * @property {boolean} STD Standard mode
	 */
	public readonly STD: boolean;

	/**
	 * @property {boolean} LP Low power mode
	 */
	public readonly LP: boolean;

	/**
	 * Constructs RF modes
	 * @param {boolean} STD Standard mode
	 * @param {boolean} LP Low power mode
	 */
	public constructor(STD: boolean, LP: boolean) {
		this.STD = STD;
		this.LP = LP;
	}

	/**
	 * Serializes RF modes from integer returned by the API
	 * @param {number} raw Raw value
	 * @return {RfModes} Deserialized RF mode
	 */
	public static deserialize(raw: number): RfModes {
		return new RfModes((raw & 0x01) === 0x01, (raw & 0x02) === 0x02);
	}

}

/**
 * Product - raw response from the API
 * @internal
 */
export interface ProductRaw {

	/**
	 * Product HWPID (Hardware Profile ID)
	 */
	hwpid: number;

	/**
	 * Product name
	 */
	name: string;

	/**
	 * Product manufacturer ID
	 */
	manufacturerID: number;

	/**
	 * Company name
	 */
	companyName: string;

	/**
	 * Product homepage
	 */
	homePage: string;

	/**
	 * Product picture
	 */
	picture: string;

	/**
	 * Product RF modes
	 */
	rfMode: number;

	/**
	 * Product metadata
	 */
	metadata: ProductMetadataRaw | Record<string, never>;
}

/**
 * Product
 */
export class Product {

	/**
	 * @property {number} hwpid Product HWPID (Hardware Profile ID)
	 */
	public readonly hwpid: number;

	/**
	 * @property {string} name Product name
	 */
	public readonly name: string;

	/**
	 * @property {number} manufacturerId Product manufacturer ID
	 */
	public readonly manufacturerId: number;

	/**
	 * @property {string} companyName Company name
	 */
	public readonly companyName: string;

	/**
	 * @property {string} homePage Product homepage
	 */
	public readonly homePage: string;

	/**
	 * @property {string} picture Product picture
	 */
	public readonly picture: string;

	/**
	 * @property {RfModes} rfModes Product RF modes
	 */
	public readonly rfModes: RfModes;

	/**
	 * @property {ProductMetadata | null} metadata Product metadata
	 */
	public readonly metadata: ProductMetadata | null;

	/**
	 * Constructs product
	 * @param {number} hwpid Product HWPID (Hardware Profile ID)
	 * @param {string} name Product name
	 * @param {number} manufacturerId Product manufacturer ID
	 * @param {string} companyName Company name
	 * @param {string} homePage Product homepage
	 * @param {string} picture Product picture
	 * @param {RfModes} rfMode Product RF modes
	 * @param {ProductMetadata | null} metadata Product metadata
	 */
	public constructor(hwpid: number, name: string, manufacturerId: number, companyName: string, homePage: string, picture: string, rfMode: RfModes, metadata: ProductMetadata | null) {
		this.hwpid = hwpid;
		this.name = name;
		this.manufacturerId = manufacturerId;
		this.companyName = companyName;
		this.homePage = homePage;
		this.picture = picture;
		this.rfModes = rfMode;
		this.metadata = metadata;
	}

	/**
	 * Serializes product from raw response from the API
	 * @param {ProductRaw} raw Raw response from the API
	 * @return {Product} Deserialized product
	 */
	public static deserialize(raw: ProductRaw): Product {
		return new Product(
			raw.hwpid,
			raw.name,
			raw.manufacturerID,
			raw.companyName,
			raw.homePage,
			raw.picture,
			RfModes.deserialize(raw.rfMode),
			Object.keys(raw.metadata).length > 0 ? ProductMetadata.deserialize(raw.metadata as ProductMetadataRaw) : null,
		);
	}

}
