/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Dpa } from './Dpa';
import { Os } from './Os';

/**
 * OS/DPA version attributes
 */
export class OsDpaAttributes {

	/**
	 * @property {boolean|null} beta Beta version flag
	 */
	public readonly beta: boolean|null;

	/**
	 * @property {boolean|null} obsolete Obsolete version flag
	 */
	public readonly obsolete: boolean|null;

	/**
	 * Constructs OS/DPA version attributes
	 * @param {boolean} beta `true` if the version is beta, `false` otherwise, `null` if it does not matter
	 * @param {boolean} obsolete `true` if the version is obsolete, `false` otherwise, `null` if it does not matter
	 */
	public constructor(beta: boolean|null, obsolete: boolean|null) {
		this.beta = beta;
		this.obsolete = obsolete;
	}

	/**
	 * Deserializes OS/DPA attributes from the API response
	 * @param {number} raw Raw OS/DPA attributes
	 * @return {OsDpaAttributes} Deserialized OS/DPA attributes
	 */
	public static deserialize(raw: number): OsDpaAttributes {
		return new OsDpaAttributes((raw & 0x01) === 0x01, (raw & 0x02) === 0x02);
	}

	/**
	 * Serializes OS/DPA attributes to the API request
	 * @return {number} Raw OS/DPA attributes
	 */
	public serialize(): number {
		return (this.beta ? 0x01 : 0x00) | (this.obsolete ? 0x02 : 0x00);
	}

	/**
	 * Serializes OS/DPA attributes mask to the API request
	 * @return {number} Raw OS/DPA attributes mask
	 */
	public serializeMask(): number {
		return (this.beta !== null ? 0x01 : 0x00) | (this.obsolete !== null ? 0x02 : 0x00);
	}

	/**
	 * Returns OS/DPA attributes as query parameters
	 * @return {Record<string, boolean>} Query parameters
	 */
	public getParams(): Record<string, boolean> {
		const params: Record<string, boolean> = {};
		if (this.beta !== null) {
			params.beta = this.beta;
		}
		if (this.obsolete !== null) {
			params.obsolete = this.obsolete;
		}
		return params;
	}

	/**
	 * Returns DPA attributes as query parameters for OS&DPA endpoint
	 * @return {Record<string, boolean>} Query parameters
	 */
	public getDpaParams(): Record<string, boolean> {
		const params: Record<string, boolean> = {};
		if (this.beta !== null) {
			params.dpaBeta = this.beta;
		}
		if (this.obsolete !== null) {
			params.dpaObsolete = this.obsolete;
		}
		return params;
	}

	/**
	 * Returns OS attributes as query parameters for OS&DPA endpoint
	 * @return {Record<string, boolean>} Query parameters
	 */
	public getOsParams(): Record<string, boolean> {
		const params: Record<string, boolean> = {};
		if (this.beta !== null) {
			params.osBeta = this.beta;
		}
		if (this.obsolete !== null) {
			params.osObsolete = this.obsolete;
		}
		return params;
	}

}

/**
 * TR module family
 */
export enum TrFamily {

	/**
	 * TR-7xD
	 */
	TR_7xD = '7xD',

	/**
	 * TR-7xG
	 */
	TR_7xG = '7xG',

}

/**
 * OS&DPA version - raw response from the API
 */
export interface OsDpaRaw {

	/**
	 * @property {number} osDpaID OS&DPA ID
	 */
	osDpaID: number;

	/**
	 * @property {string} os IQRF OS build
	 */
	os: string;

	/**
	 * @property {string} osVersion IQRF OS version
	 */
	osVersion: string;

	/**
	 * @property {string} osTrFamily TR module family
	 */
	osTrFamily: string;

	/**
	 * @property {number} osAttributes IQRF OS attributes
	 */
	osAttributes: number;

	/**
	 * @property {string} osDownloadPath IQRF OS download base path
	 */
	osDownloadPath: string;

	/**
	 * @property {string} dpa DPA version
	 */
	dpa: string;

	/**
	 * @property {number} dpaAttributes DPA attributes
	 */
	dpaAttributes: number;

	/**
	 * @property {string} dpaDownloadPath DPA download base path
	 */
	dpaDownloadPath: string;

	/**
	 * @property {string} downloadPath DPA download base path
	 * @deprecated Use @link{OsDpa::dpaDownloadPath} instead
	 */
	downloadPath: string;

	/**
	 * @property {string} notes Notes
	 */
	notes: string;

	/**
	 * @property {number[]} compatibility Compatibility list
	 */
	compatibility: number[];

}

/**
 * IQRF OS&DPA version
 */
export class OsDpa {

	/**
	 * @property {number} id IQRF OS&DPA ID
	 */
	public readonly id: number;

	/**
	 * @property {Dpa} dpa DPA version
	 */
	public readonly dpa: Dpa;

	/**
	 * @property {Os} os IQRF OS version
	 */
	public readonly os: Os;

	/**
	 * @property {string} notes Notes
	 */
	public readonly notes: string;

	/**
	 * @property {number[]} compatibility Compatibility list
	 */
	public readonly compatibility: number[];

	/**
	 * Constructs IQRF OS&DPA version
	 * @param {number} id IQRF OS&DPA ID
	 * @param {Dpa} dpa DPA version
	 * @param {Os} os IQRF OS version
	 * @param {string} notes Notes
	 * @param {number[]} compatibility Compatibility list
	 */
	public constructor(id: number, dpa: Dpa, os: Os, notes: string, compatibility: number[]) {
		this.id = id;
		this.dpa = dpa;
		this.os = os;
		this.notes = notes;
		this.compatibility = compatibility;
	}

	/**
	 * Deserializes OS&DPA version from raw response from the API
	 * @param {OsDpaRaw} raw Raw response from the API
	 * @return {OsDpa} Deserialized OS&DPA
	 */
	public static deserialize(raw: OsDpaRaw): OsDpa {
		return new OsDpa(raw.osDpaID, Dpa.deserializeOsDpa(raw), Os.deserializeOsDpa(raw), raw.notes, raw.compatibility);
	}

}
