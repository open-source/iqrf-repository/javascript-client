/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { type AxiosResponse } from 'axios';

import { Company, type CompanyRaw } from '../types';

import { BaseService } from './BaseService';

/**
 * Company service
 */
export class CompanyService extends BaseService {

	/**
	 * Lists all companies
	 * @return {Promise<Company[]>} Companies
	 */
	public async list(): Promise<Company[]> {
		const response: AxiosResponse<CompanyRaw[]> =
			await this.axiosInstance.get('/companies');
		return response.data.map((raw: CompanyRaw): Company => Company.deserialize(raw));
	}

	/**
	 * Returns company by its ID
	 * @param {number} id Company ID
	 * @return {Promise<Company>} Company
	 */
	public async get(id: number): Promise<Company> {
		const response: AxiosResponse<CompanyRaw> =
			await this.axiosInstance.get(`/companies/${id.toString()}`);
		return Company.deserialize(response.data);
	}

}

