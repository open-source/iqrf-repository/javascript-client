/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { type AxiosRequestConfig, type AxiosResponse } from 'axios';

import { Os, type OsDpaAttributes, type OsRaw } from '../types';

import { BaseService } from './BaseService';

/**
 * IQRF OS service
 */
export class OsService extends BaseService {

	/**
	 * Lists all IQRF OS versions
	 * @param {OsDpaAttributes|null} attributes IQRF OS attributes
	 * @return {Promise<Os[]>} IQRF OS versions
	 */
	public async list(attributes: OsDpaAttributes|null = null): Promise<Os[]> {
		const config: AxiosRequestConfig = {};
		if (attributes) {
			config.params = attributes.getParams();
		}
		const response: AxiosResponse<OsRaw[]> =
			await this.axiosInstance.get('/os', config);
		return response.data.map((raw: OsRaw): Os => Os.deserialize(raw));
	}

	/**
	 * Returns IQRF OS version by its build
	 * @param {string} build IQRF OS build
	 * @return {Promise<Os>} IQRF OS
	 */
	public async get(build: string): Promise<Os> {
		if (!/^[\da-f]{4}$/i.test(build)) {
			throw new Error('Invalid IQRF OS build format');
		}
		const response: AxiosResponse<OsRaw> =
			await this.axiosInstance.get('/os', { params: { build: build } });
		return Os.deserialize(response.data);
	}

}

