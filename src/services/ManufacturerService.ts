/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { type AxiosResponse } from 'axios';

import { Manufacturer, type ManufacturerRaw } from '../types';

import { BaseService } from './BaseService';

/**
 * Manufacturer service
 */
export class ManufacturerService extends BaseService {

	/**
	 * Lists all manufacturers
	 * @return {Promise<Manufacturer[]>} Companies
	 */
	public async list(): Promise<Manufacturer[]> {
		const response: AxiosResponse<ManufacturerRaw[]> =
			await this.axiosInstance.get('/manufacturers');
		return response.data.map((raw: ManufacturerRaw): Manufacturer => Manufacturer.deserialize(raw));
	}

	/**
	 * Returns manufacturer by its ID
	 * @param {number} id Manufacturer ID
	 * @return {Promise<Manufacturer>} Manufacturer
	 */
	public async get(id: number): Promise<Manufacturer> {
		const response: AxiosResponse<ManufacturerRaw> =
			await this.axiosInstance.get(`/manufacturers/${id.toString()}`);
		return Manufacturer.deserialize(response.data);
	}

}

