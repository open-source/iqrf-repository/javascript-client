/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { type AxiosRequestConfig, type AxiosResponse } from 'axios';

import { Dpa, type DpaRaw, type OsDpaAttributes } from '../types';

import { BaseService } from './BaseService';

/**
 * DPA service
 */
export class DpaService extends BaseService {

	/**
	 * Lists all DPA versions
	 * @param {OsDpaAttributes|null} attributes DPA attributes
	 * @return {Promise<Dpa[]>} DPA versions
	 */
	public async list(attributes: OsDpaAttributes|null = null): Promise<Dpa[]> {
		const config: AxiosRequestConfig = {};
		if (attributes) {
			config.params = attributes.getParams();
		}
		const response: AxiosResponse<DpaRaw[]> =
			await this.axiosInstance.get('/dpa', config);
		return response.data.map((raw: DpaRaw): Dpa => Dpa.deserialize(raw));
	}

	/**
	 * Returns DPA by its version
	 * @param {string} version DPA version
	 * @return {Promise<Dpa>} DPA
	 */
	public async get(version: string): Promise<Dpa> {
		const versionParts: string[] = version.split('.', 2);
		if (versionParts.length !== 2 ||
			!['1', '2'].includes(versionParts[0].length.toString()) ||
			versionParts[1].length !== 2
		) {
			throw new Error('Invalid DPA version format');
		}
		const versionMajor: string = Number.parseInt(versionParts[0]).toString().padStart(2, '0');
		const versionMinor: string = Number.parseInt(versionParts[1]).toString().padStart(2, '0');
		const response: AxiosResponse<DpaRaw> = await this.axiosInstance.get('/dpa', {
			params: { version: `${versionMajor}${versionMinor}` },
		});
		return Dpa.deserialize(response.data);
	}

}

