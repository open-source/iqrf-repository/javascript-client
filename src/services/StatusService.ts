/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { type AxiosResponse } from 'axios';

import { Status, type StatusRaw, transformJsonBigInt } from '../types';

import { BaseService } from './BaseService';

/**
 * Status service
 */
export class StatusService extends BaseService {

	/**
	 * Get repository status
	 * @return {Promise<Status>} Status
	 */
	public async get(): Promise<Status> {
		const response: AxiosResponse<StatusRaw> =
			await this.axiosInstance.get('/server', {
				transformResponse: transformJsonBigInt,
			});
		return Status.deserialize(response.data);
	}
}
