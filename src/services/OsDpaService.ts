/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { type AxiosRequestConfig, type AxiosResponse } from 'axios';

import { OsDpa, type OsDpaAttributes, type OsDpaRaw } from '../types';

import { BaseService } from './BaseService';

/**
 * IQRF OS & DPA request parameters
 */
export interface OsDpaParams {

	/**
	 * IQRF OS attributes
	 */
	osAttributes?: OsDpaAttributes|null,

	/**
	 * IQRF OS build
	 */
	osBuild?: string|null,

	/**
	 * DPA attributes
	 */
	dpaAttributes?: OsDpaAttributes|null,

	/**
	 * DPA version
	 */
	dpaVersion?: string|null,

}

/**
 * IQRF OS & DPA service
 */
export class OsDpaService extends BaseService {

	/**
	 * Lists all IQRF OS & DPA versions
	 * @param {{osAttributes, osBuild, dpaAttributes, dpaVersion}} __namedParameters IQRF OS & DPA request parameters
	 * @param {OsDpaAttributes|null} __namedParameters.osAttributes IQRF OS attributes
	 * @param {string|null} __namedParameters.osBuild IQRF OS build
	 * @param {OsDpaAttributes|null} __namedParameters.dpaAttributes DPA attributes
	 * @param {string|null} __namedParameters.dpaVersion DPA version
	 * @return {Promise<OsDpa[]>} IQRF OS & DPA versions
	 */
	public async list(
		{
			osAttributes = null,
			osBuild = null,
			dpaAttributes = null,
			dpaVersion = null,
		}: {
			osAttributes?: OsDpaAttributes|null,
			osBuild?: string|null,
			dpaAttributes?: OsDpaAttributes|null,
			dpaVersion?: string|null,
		} = {},
	): Promise<OsDpa[]> {
		const config: AxiosRequestConfig = {};
		let params: Record<string, boolean|number|string> = {};
		if (osAttributes) {
			params = { ...params, ...osAttributes.getOsParams() };
		}
		if (osBuild) {
			params.os = osBuild;
		}
		if (dpaAttributes) {
			params = { ...params, ...dpaAttributes.getDpaParams() };
		}
		if (dpaVersion) {
			params.dpa = dpaVersion;
		}
		if (Object.keys(params).length > 0) {
			config.params = params;
		}
		const response: AxiosResponse<OsDpaRaw[]> =
			await this.axiosInstance.get('/osdpa', config);
		return response.data.map((raw: OsDpaRaw): OsDpa => OsDpa.deserialize(raw));
	}

}

