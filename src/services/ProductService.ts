/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { type AxiosResponse } from 'axios';

import { Product, type ProductRaw } from '../types';

import { BaseService } from './BaseService';

/**
 * Product service
 */
export class ProductService extends BaseService {

	/**
	 * Lists all products
	 * @return {Promise<Product[]>} Products
	 */
	public async list(): Promise<Product[]> {
		const response: AxiosResponse<ProductRaw[]> =
			await this.axiosInstance.get('/products');
		return response.data.map((raw: ProductRaw): Product => Product.deserialize(raw));
	}

	/**
	 * Returns product by its ID
	 * @param {number} id Product ID
	 * @return {Promise<Product>} Product
	 */
	public async get(id: number): Promise<Product> {
		const response: AxiosResponse<ProductRaw> =
			await this.axiosInstance.get(`/products/${id.toString()}`);
		return Product.deserialize(response.data);
	}

}

