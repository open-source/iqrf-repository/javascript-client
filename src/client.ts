/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import axios, {
	type AxiosBasicCredentials,
	type AxiosInstance,
	type AxiosInterceptorOptions,
	type AxiosRequestConfig,
	type AxiosResponse,
	type InternalAxiosRequestConfig,
} from 'axios';

import {
	CompanyService,
	DpaService,
	ManufacturerService,
	OsDpaService,
	OsService,
	ProductService,
	StatusService,
} from './services';
import { transformJsonBigInt } from './types';

/**
 * IQRF Repository API client credentials
 */
export interface ClientCredentials {

	/**
	 * Username
	 */
	username: string|null;

	/**
	 * Password
	 */
	password: string|null;

}

/**
 * IQRF Repository API client options
 */
export interface ClientOptions {

	/**
	 * Axios instance
	 * @see https://axios-http.com/docs/instance
	 */
	axiosInstance?: AxiosInstance;

	/**
	 * Axios instance configuration
	 * @see https://axios-http.com/docs/req_config
	 */
	config?: AxiosRequestConfig;

	/**
	 * Client credentials
	 */
	credentials?: ClientCredentials;

}

/**
 * # IQRF Repository API client class
 *
 * Client is a wrapper around API methods of IQRF Repository providing shared configuration for Axios instance.
 *
 * ## Instantiate with defaults
 * ```typescript
 * const client = new Client();
 * ```
 *
 * ## Instantiate with custom configuration
 * ```typescript
 * const config: AxiosRequestConfig = {
 * 	baseURL: 'https://devrepo.iqrfalliance.org/api/',
 * };
 * const client = new Client({config});
 * ```
 *
 * ## Instantiate with custom Axios instance **advanced**
 * ```typescript
 * const config: AxiosRequestConfig = {
 * 	baseURL: 'https://repository.iqrfalliance.org/api/',
 * }
 * const axiosInstance = axios.create(config);
 * const client = new Client({axiosInstance});
 */
export class Client {

	/**
	 * Axios instance
	 * @private
	 */
	private readonly axiosInstance: AxiosInstance;

	/**
	 * Default Axios instance configuration
	 * @private
	 */
	private readonly defaultAxiosConfig: AxiosRequestConfig = {
		/** Production IQRF Repository API base URL */
		baseURL: 'https://repository.iqrfalliance.org/api/',
		/** Timeout in milliseconds */
		timeout: 10_000,
		transformResponse: transformJsonBigInt,
	};

	/**
	 * Constructs IQRF Repository API client
	 * @param {{axiosInstance, config, credentials}} __namedParameters Client options
	 * @param {AxiosInstance} __namedParameters.axiosInstance Axios instance, @see https://axios-http.com/docs/instance
	 * @param {AxiosRequestConfig} __namedParameters.config Axios instance configuration, @see https://axios-http.com/docs/req_config
	 * @param {ClientCredentials} __namedParameters.credentials Client credentials
	 */
	public constructor({ axiosInstance, config, credentials }: ClientOptions = {}) {
		if (axiosInstance && config) {
			throw new Error('Cannot instantiate Client with both axiosInstance and config.');
		}
		if (axiosInstance) {
			this.axiosInstance = axiosInstance;
		} else if (config) {
			config = {
				...this.defaultAxiosConfig,
				...config,
			};
			this.axiosInstance = axios.create(config);
		} else {
			this.axiosInstance = axios.create(this.defaultAxiosConfig);
		}
		if (credentials?.username && credentials.password) {
			this.axiosInstance.defaults.auth = credentials as AxiosBasicCredentials;
		}
	}

	/**
	 * Returns Axios instance
	 * @internal
	 * @return {AxiosInstance} Axios instance
	 */
	public getAxiosInstance(): AxiosInstance {
		return this.axiosInstance;
	}

	/**
	 * Returns company service
	 * @return {CompanyService} Company service
	 */
	public getCompanyService(): CompanyService {
		return new CompanyService(this);
	}

	/**
	 * Returns DPA version service
	 * @return {DpaService} DPA version service
	 */
	public getDpaService(): DpaService {
		return new DpaService(this);
	}

	/**
	 * Returns manufacturer service
	 * @return {ManufacturerService} Manufacturer service
	 */
	public getManufacturerService(): ManufacturerService {
		return new ManufacturerService(this);
	}

	/**
	 * Returns IQRF OS & DPA version service
	 * @return {OsDpaService} IQRF OS & DPA version service
	 */
	public getOsDpaService(): OsDpaService {
		return new OsDpaService(this);
	}

	/**
	 * Returns IQRF OS version service
	 * @return {OsService} IQRF OS version service
	 */
	public getOsService(): OsService {
		return new OsService(this);
	}

	/**
	 * Returns product service
	 * @return {ProductService} Product service
	 */
	public getProductService(): ProductService {
		return new ProductService(this);
	}

	/**
	 * Returns status service
	 * @return {StatusService} Status service
	 */
	public getStatusService(): StatusService {
		return new StatusService(this);
	}

	/**
	 * Checks if client has credentials set
	 * @return {boolean} `true` if client has credentials set, `false` otherwise
	 */
	public hasCredentials(): boolean {
		return this.axiosInstance.defaults.auth !== undefined;
	}

	/**
	 * Adds a request interceptor
	 * @param {Function} onFulfilled Fulfilled callback
	 * @param {Function} onRejected Rejected callback
	 * @param {AxiosInterceptorOptions} options Interceptor options
	 * @return {number} Interceptor ID
	 */
	public useRequestInterceptor(onFulfilled?: ((value: InternalAxiosRequestConfig) => InternalAxiosRequestConfig | Promise<InternalAxiosRequestConfig>) | null, onRejected?: ((error: any) => any) | null, options?: AxiosInterceptorOptions): number {
		return this.axiosInstance.interceptors.request.use(onFulfilled, onRejected, options);
	}

	/**
	 * Adds a response interceptor
	 * @param {Function} onFulfilled Fulfilled callback
	 * @param {Function} onRejected Rejected callback
	 * @return {number} Interceptor ID
	 */
	public useResponseInterceptor(onFulfilled?: ((value: AxiosResponse) => AxiosResponse | Promise<AxiosResponse>) | null, onRejected?: ((error: any) => any) | null): number {
		return this.axiosInstance.interceptors.response.use(onFulfilled, onRejected);
	}

	/**
	 * Ejects a request interceptor
	 * @param {number} interceptorId Interceptor ID
	 */
	public ejectRequestInterceptor(interceptorId: number): void {
		this.axiosInstance.interceptors.request.eject(interceptorId);
	}

	/**
	 * Ejects a response interceptor
	 * @param {number} interceptorId Interceptor ID
	 */
	public ejectResponseInterceptor(interceptorId: number): void {
		this.axiosInstance.interceptors.response.eject(interceptorId);
	}

	/**
	 * Clears all request interceptors
	 */
	public clearRequestInterceptors(): void {
		this.axiosInstance.interceptors.request.clear();
	}

	/**
	 * Clears all response interceptors
	 */
	public clearResponseInterceptors(): void {
		this.axiosInstance.interceptors.response.clear();
	}

}
