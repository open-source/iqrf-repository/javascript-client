/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import axios, {
	type AxiosBasicCredentials,
	type AxiosInstance,
	type AxiosRequestConfig,
} from 'axios';
import { beforeEach, describe, expect, it } from 'vitest';

import { Client } from '../src';
import {
	CompanyService,
	DpaService,
	ManufacturerService,
	OsDpaService,
	OsService,
	ProductService,
	StatusService,
} from '../src/services';

describe('Client', (): void => {

	/**
	 * @var {Client} client Client instance
	 */
	let client: Client;

	beforeEach((): void => {
		client = new Client();
	});

	it('can be instantiated', (): void => {
		expect.assertions(5);
		expect(client.getAxiosInstance())
			.toBeDefined();
		expect(client.getAxiosInstance().defaults.auth)
			.toBeUndefined();
		expect(client.getAxiosInstance().defaults.baseURL)
			.toBe('https://repository.iqrfalliance.org/api/');
		expect(client.getAxiosInstance().defaults.timeout)
			.toBe(10_000);
		expect(client.hasCredentials()).toBeFalsy();
	});

	it('can be instantiated with custom Axios instance', (): void => {
		expect.assertions(5);
		const config: AxiosRequestConfig = {
			baseURL: 'https://devrepo.iqrfalliance.org/api/',
			timeout: 5_000,
		};
		const axiosInstance: AxiosInstance = axios.create(config);
		client = new Client({ axiosInstance: axiosInstance });
		expect(client.getAxiosInstance())
			.toBeDefined();
		expect(client.getAxiosInstance().defaults.auth)
			.toBeUndefined();
		expect(client.getAxiosInstance().defaults.baseURL)
			.toBe('https://devrepo.iqrfalliance.org/api/');
		expect(client.getAxiosInstance().defaults.timeout)
			.toBe(5_000);
		expect(client.hasCredentials())
			.toBeFalsy();
	});

	it('can be instantiated with custom Axios instance configuration', (): void => {
		expect.assertions(5);
		const config: AxiosRequestConfig = {
			baseURL: 'https://devrepo.iqrfalliance.org/api/',
			timeout: 5_000,
		};
		client = new Client({ config: config });
		expect(client.getAxiosInstance())
			.toBeDefined();
		expect(client.getAxiosInstance().defaults.auth)
			.toBeUndefined();
		expect(client.getAxiosInstance().defaults.baseURL)
			.toBe('https://devrepo.iqrfalliance.org/api/');
		expect(client.getAxiosInstance().defaults.timeout)
			.toBe(5_000);
		expect(client.hasCredentials())
			.toBeFalsy();
	});

	it('can be instantiated with credentials', (): void => {
		expect.assertions(5);
		const credentials: AxiosBasicCredentials = {
			username: 'username',
			password: 'password',
		};
		client = new Client({ credentials });
		expect(client.getAxiosInstance())
			.toBeDefined();
		expect(client.getAxiosInstance().defaults.auth)
			.toStrictEqual(credentials);
		expect(client.getAxiosInstance().defaults.baseURL)
			.toBe('https://repository.iqrfalliance.org/api/');
		expect(client.getAxiosInstance().defaults.timeout)
			.toBe(10_000);
		expect(client.hasCredentials())
			.toBeTruthy();
	});

	it('cannot be instantiated with custom Axios instance and custom Axios instance configuration', (): void => {
		expect.assertions(1);
		const config: AxiosRequestConfig = {
			baseURL: 'https://devrepo.iqrfalliance.org/api/',
			timeout: 5_000,
		};
		const axiosInstance: AxiosInstance = axios.create(config);
		expect(() => new Client({ axiosInstance: axiosInstance, config: config }))
			.toThrow('Cannot instantiate Client with both axiosInstance and config.');
	});

	it('return Company service', (): void => {
		expect.assertions(1);
		expect(client.getCompanyService())
			.toBeInstanceOf(CompanyService);
	});

	it('return DPA version service', (): void => {
		expect.assertions(1);
		expect(client.getDpaService())
			.toBeInstanceOf(DpaService);
	});

	it('return Manufacturer service', (): void => {
		expect.assertions(1);
		expect(client.getManufacturerService())
			.toBeInstanceOf(ManufacturerService);
	});

	it('return IQRF OS version service', (): void => {
		expect.assertions(1);
		expect(client.getOsService())
			.toBeInstanceOf(OsService);
	});

	it('return IQRF OS & DPA version service', (): void => {
		expect.assertions(1);
		expect(client.getOsDpaService())
			.toBeInstanceOf(OsDpaService);
	});

	it('return Product service', (): void => {
		expect.assertions(1);
		expect(client.getProductService())
			.toBeInstanceOf(ProductService);
	});

	it('return Status service', (): void => {
		expect.assertions(1);
		expect(client.getStatusService())
			.toBeInstanceOf(StatusService);
	});

});
