/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { beforeEach, describe, expect, it } from 'vitest';

import { CompanyService } from '../../src/services';
import { Company, type CompanyRaw } from '../../src/types';
import { mockedAxios, mockedClient } from '../mocks/axios';

describe('CompanyService', (): void => {

	/**
	 * @const {CompanyService} service Company service
	 */
	const service: CompanyService = new CompanyService(mockedClient);

	/**
	 * @const {CompanyRaw[]} rawCompanies Raw companies response
	 */
	const rawCompanies: CompanyRaw[] = [
		{
			'companyID': 2,
			'name': 'IQRF Tech s.r.o.',
			'homePage': 'https://www.iqrf.org/',
		},
	];

	/**
	 * @const {Company[]} companies Companies
	 */
	const companies: Company[] = [
		new Company(2, 'IQRF Tech s.r.o.', 'https://www.iqrf.org/'),
	];

	beforeEach((): void => {
		mockedAxios.reset();
	});

	it('list all companies', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/companies')
			.replyOnce(200, rawCompanies);
		const actual: Company[] = await service.list();
		expect(actual).toStrictEqual(companies);
	});

	it('get company by ID', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/companies/2')
			.replyOnce(200, rawCompanies[0]);
		const actual: Company = await service.get(2);
		expect(actual).toStrictEqual(companies[0]);
	});

});
