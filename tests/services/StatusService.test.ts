/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { type AxiosRequestConfig } from 'axios';
import JsonBigInt from 'json-bigint';
import { DateTime } from 'luxon';
import { beforeEach, describe, expect, it } from 'vitest';

import { StatusService } from '../../src/services';
import { Status, type StatusRaw, transformJsonBigInt } from '../../src/types';
import { mockedAxios, mockedClient } from '../mocks/axios';

describe('StatusService', (): void => {

	/**
	 * @const {StatusService} service Status service
	 */
	const service: StatusService = new StatusService(mockedClient);

	/**
	 * @const {StatusRaw} rawStatus Raw status response
	 */
	const rawStatus: StatusRaw = {
		apiVersion: 0,
		hostname: 'repository.iqrfalliance.org',
		user: 'guest',
		buildDateTime: '2024-04-25T08:35:49+02:00',
		startDateTime: '2024-06-13T09:50:23+02:00',
		dateTime: '2024-07-02T10:25:55+02:00',
		databaseChecksum: 1_122_653_398_629_648_527n,
		databaseChangeDateTime: '2024-06-19T11:48:26+02:00',
	};

	/**
	 * @const {StatusRaw} rawStatusStringChecksum Raw status response with string checksum
	 */
	const rawStatusStringChecksum: StatusRaw = {
		apiVersion: 0,
		hostname: 'repository.iqrfalliance.org',
		user: 'guest',
		buildDateTime: '2024-04-25T08:35:49+02:00',
		startDateTime: '2024-06-13T09:50:23+02:00',
		dateTime: '2024-07-02T10:25:55+02:00',
		databaseChecksum: '1122653398629648527',
		databaseChangeDateTime: '2024-06-19T11:48:26+02:00',
	};

	/**
	 * @const {Status} status Status
	 */
	const status: Status = new Status(
		0,
		'repository.iqrfalliance.org',
		'guest',
		DateTime.fromISO('2024-04-25T08:35:49+02:00'),
		DateTime.fromISO('2024-06-13T09:50:23+02:00'),
		DateTime.fromISO('2024-07-02T10:25:55+02:00'),
		1_122_653_398_629_648_527n,
		DateTime.fromISO('2024-06-19T11:48:26+02:00'),
	);

	beforeEach((): void => {
		mockedAxios.reset();
		global.JSON.parse = JsonBigInt({ useNativeBigInt: true }).parse;
		global.JSON.stringify = JsonBigInt({ useNativeBigInt: true }).stringify;
	});

	it('get status', async (): Promise<void> => {
		expect.assertions(2);
		mockedAxios.onGet('/server').replyOnce(function (config: AxiosRequestConfig) {
			expect(config.transformResponse).toStrictEqual(transformJsonBigInt);
			return [200, rawStatus];
		});
		const actual: Status = await service.get();
		expect(actual).toStrictEqual(status);
	});

	it('get status string checksum', async (): Promise<void> => {
		expect.assertions(2);
		mockedAxios.onGet('/server').replyOnce(function (config: AxiosRequestConfig) {
			expect(config.transformResponse).toStrictEqual(transformJsonBigInt);
			return [200, rawStatusStringChecksum];
		});
		const actual: Status = await service.get();
		expect(actual).toStrictEqual(status);
	});

});
