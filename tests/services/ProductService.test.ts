/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { beforeEach, describe, expect, it } from 'vitest';

import { ProductService } from '../../src/services';
import {
	AccumulatorPower,
	BatteryPower,
	HwpidVersions,
	MetadataProfile,
	PowerSupply,
	Product,
	ProductMetadata,
	type ProductRaw,
	RfModes,
	StaticMetadata,
} from '../../src/types';
import { mockedAxios, mockedClient } from '../mocks/axios';

describe('ProductService', (): void => {

	/**
	 * @const {ProductService} service Product service
	 */
	const service: ProductService = new ProductService(mockedClient);

	/**
	 * @const {ProductRaw[]} rawProducts Raw product response
	 */
	const rawProducts: ProductRaw[] = [
		{
			'hwpid': 1_026,
			'name': 'DDC-SE01 + DDC-RE01 sensor example',
			'manufacturerID': 2,
			'companyName': 'IQRF Tech s.r.o.',
			'homePage': 'https://www.iqrf.org/products/development-tools/development-kits',
			'picture': 'https://repository.iqrfalliance.org/productpictures/0002.png',
			'rfMode': 1,
			'metadata': {
				'static': [{
					'version': 0,
					'profiles': [{
						'hwpidVersions': {
							'min': 0,
							'max': -1,
						},
						'routing': true,
						'beaming': false,
						'repeater': false,
						'frcAggregation': false,
						'iqarosCompatible': true,
						'iqrfSensor': [
							1,
							129,
							129,
						],
						'iqrfBinaryOutput': 4,
						'powerSupply': {
							'mains': false,
							'accumulator': {
								'present': true,
								'type': 'LIP552240',
								'lowLevel': 3_400,
							},
							'battery': {
								'present': false,
								'type': null,
								'changeThreshold': null,
							},
							'minVoltage': 2_200,
						},
					}],
				}],
				'extra': [
					{
						'version': 0,
						'productionTest': [],
					},
				],
			},
		},
		{
			'hwpid': 5_122,
			'name': 'Sensor template',
			'manufacturerID': 2,
			'companyName': 'IQRF Tech s.r.o.',
			'homePage': 'https://www.iqrfalliance.org/marketplace/sensor-template',
			'picture': 'https://repository.iqrfalliance.org/productpictures/1402.jpg',
			'rfMode': 1,
			'metadata': {},
		},
	];

	/**
	 * @const {Product[]} products Products
	 */
	const products: Product[] = [
		new Product(
			1_026,
			'DDC-SE01 + DDC-RE01 sensor example',
			2,
			'IQRF Tech s.r.o.',
			'https://www.iqrf.org/products/development-tools/development-kits',
			'https://repository.iqrfalliance.org/productpictures/0002.png',
			new RfModes(true, false),
			new ProductMetadata(
				[new StaticMetadata(
					0,
					[new MetadataProfile(
						new HwpidVersions(0, -1),
						true,
						false,
						false,
						false,
						true,
						[1, 129, 129],
						4,
						new PowerSupply(
							false,
							new AccumulatorPower(true, 'LIP552240', 3_400),
							new BatteryPower(false, null, null),
							2_200,
						),
					)],
				)],
				[
					{
						'version': 0,
						'productionTest': [],
					},
				],
			),
		),
		new Product(
			5_122,
			'Sensor template',
			2,
			'IQRF Tech s.r.o.',
			'https://www.iqrfalliance.org/marketplace/sensor-template',
			'https://repository.iqrfalliance.org/productpictures/1402.jpg',
			new RfModes(true, false),
			null,
		),
	];

	beforeEach((): void => {
		mockedAxios.reset();
	});

	it('list all products', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/products')
			.replyOnce(200, rawProducts);
		const actual: Product[] = await service.list();
		expect(actual).toStrictEqual(products);
	});

	it('get product with metadata by HWPID', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/products/1026')
			.replyOnce(200, rawProducts[0]);
		const actual: Product = await service.get(1_026);
		expect(actual).toStrictEqual(products[0]);
	});

	it ('get product without metadata by HWPID', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/products/5122')
			.replyOnce(200, rawProducts[1]);
		const actual: Product = await service.get(5_122);
		expect(actual).toStrictEqual(products[1]);
	});

});
