/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { beforeEach, describe, expect, it } from 'vitest';

import { ManufacturerService } from '../../src/services';
import { Manufacturer, type ManufacturerRaw } from '../../src/types';
import { mockedAxios, mockedClient } from '../mocks/axios';

describe('CompanyService', (): void => {

	/**
	 * @const {ManufacturerService} service Manufacturer service
	 */
	const service: ManufacturerService = new ManufacturerService(mockedClient);

	/**
	 * @const {ManufacturerRaw[]} rawManufacturers Raw manufacturers response
	 */
	const rawManufacturers: ManufacturerRaw[] = [
		{
			'manufacturerID': 2,
			'companyID': 2,
			'name': 'IQRF Tech s.r.o.',
		},
	];

	/**
	 * @const {Manufacturer[]} manufacturers Manufacturers
	 */
	const manufacturers: Manufacturer[] = [
		new Manufacturer(2, 'IQRF Tech s.r.o.', 2),
	];

	beforeEach((): void => {
		mockedAxios.reset();
	});

	it('list all manufacturers', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/manufacturers')
			.replyOnce(200, rawManufacturers);
		const actual: Manufacturer[] = await service.list();
		expect(actual).toStrictEqual(manufacturers);
	});

	it('get manufacturer by ID', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/manufacturers/2')
			.replyOnce(200, rawManufacturers[0]);
		const actual: Manufacturer = await service.get(2);
		expect(actual).toStrictEqual(manufacturers[0]);
	});

});
