/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { beforeEach, describe, expect, it } from 'vitest';

import { OsDpaService } from '../../src/services';
import { Dpa, Os, OsDpa, OsDpaAttributes, type OsDpaRaw, TrFamily } from '../../src/types';
import { mockedAxios, mockedClient } from '../mocks/axios';

describe('OsDpaService', (): void => {

	/**
	 * @const {OsDpaService} service IQRF OS + DPA service
	 */
	const service: OsDpaService = new OsDpaService(mockedClient);

	/**
	 * @const {OsDpaRaw[]} rawVersions Raw response from the API
	 */
	const rawVersions: OsDpaRaw[] = [
		{
			'osDpaID': 27,
			'os': '08D8',
			'osVersion': '4.06D',
			'osTrFamily': '7xD',
			'osAttributes': 0,
			'osDownloadPath': 'https://repository.iqrfalliance.org/download/iqrfos/08D8',
			'dpa': '0417',
			'dpaAttributes': 0,
			'dpaDownloadPath': 'https://repository.iqrfalliance.org/download/dpa/4.17',
			'downloadPath': 'https://repository.iqrfalliance.org/download/dpa/4.17',
			'notes': 'IQRF OS 4.06D, DPA 4.17',
			'compatibility': [
				27,
				28,
				30,
			],
		},
		{
			'osDpaID': 28,
			'os': '090F',
			'osVersion': '4.06G',
			'osTrFamily': '7xG',
			'osAttributes': 0,
			'osDownloadPath': 'https://repository.iqrfalliance.org/download/iqrfos/090F',
			'dpa': '0418',
			'dpaAttributes': 0,
			'dpaDownloadPath': 'https://repository.iqrfalliance.org/download/dpa/4.18',
			'downloadPath': 'https://repository.iqrfalliance.org/download/dpa/4.18',
			'notes': 'IQRF OS 4.06G, DPA 4.18',
			'compatibility': [
				28,
				27,
				30,
			],
		},
		{
			'osDpaID': 30,
			'os': '090F',
			'osVersion': '4.06G',
			'osTrFamily': '7xG',
			'osAttributes': 0,
			'osDownloadPath': 'https://repository.iqrfalliance.org/download/iqrfos/090F',
			'dpa': '0430',
			'dpaAttributes': 0,
			'dpaDownloadPath': 'https://repository.iqrfalliance.org/download/dpa/4.30',
			'downloadPath': 'https://repository.iqrfalliance.org/download/dpa/4.30',
			'notes': 'IQRF OS 4.06G, DPA 4.30',
			'compatibility': [
				30,
				27,
				28,
			],
		},
	];

	/**
	 * @const {Dpa[]} dpaVersions DPA versions
	 */
	const dpaVersions: Dpa[] = [
		new Dpa('4.17', new OsDpaAttributes(false, false), 'https://repository.iqrfalliance.org/download/dpa/4.17'),
		new Dpa('4.18', new OsDpaAttributes(false, false), 'https://repository.iqrfalliance.org/download/dpa/4.18'),
		new Dpa('4.30', new OsDpaAttributes(false, false), 'https://repository.iqrfalliance.org/download/dpa/4.30'),
	];

	/**
	 * @const {Os[]} osVersions IQRF OS versions
	 */
	const osVersions: Os[] = [
		new Os('08D8', '4.06D', TrFamily.TR_7xD, new OsDpaAttributes(false, false), 'https://repository.iqrfalliance.org/download/iqrfos/08D8'),
		new Os('090F', '4.06G', TrFamily.TR_7xG, new OsDpaAttributes(false, false), 'https://repository.iqrfalliance.org/download/iqrfos/090F'),
	];

	/**
	 * @const {OsDpa[]} versions IQRF OS & DPA versions
	 */
	const versions: OsDpa[] = [
		new OsDpa(27, dpaVersions[0], osVersions[0], 'IQRF OS 4.06D, DPA 4.17', [27, 28, 30]),
		new OsDpa(28, dpaVersions[1], osVersions[1], 'IQRF OS 4.06G, DPA 4.18', [28, 27, 30]),
		new OsDpa(30, dpaVersions[2], osVersions[1], 'IQRF OS 4.06G, DPA 4.30', [30, 27, 28]),
	];

	beforeEach((): void => {
		mockedAxios.reset();
	});

	it('list all IQRF OS & DPA versions', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/osdpa')
			.replyOnce(200, rawVersions);
		const actual: OsDpa[] = await service.list();
		expect(actual).toStrictEqual(versions);
	});

	it('list IQRF OS & DPA versions for IQRF OS build 090F', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/osdpa', { params: { os: '090F' } })
			.replyOnce(200, [rawVersions[1], rawVersions[2]]);
		const actual: OsDpa[] = await service.list({ osBuild: '090F' });
		expect(actual).toStrictEqual([versions[1], versions[2]]);
	});

});
