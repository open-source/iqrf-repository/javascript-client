/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { beforeEach, describe, expect, it } from 'vitest';

import { DpaService } from '../../src/services';
import { Dpa, type DpaRaw, OsDpaAttributes } from '../../src/types';
import { mockedAxios, mockedClient } from '../mocks/axios';

describe('DpaService', (): void => {

	/**
	 * @const {DpaService} service DPA service
	 */
	const service: DpaService = new DpaService(mockedClient);

	/**
	 * @const {DpaRaw[]} rawVersions Raw response from the API
	 */
	const rawVersions: DpaRaw[] = [
		{
			'version': '0403',
			'attributes': 2,
			'downloadPath': 'https://repository.iqrfalliance.org/download/dpa/4.03',
		},
		{
			'version': '0410',
			'attributes': 0,
			'downloadPath': 'https://repository.iqrfalliance.org/download/dpa/4.10',
		},
	];

	/**
	 * @const {Dpa[]} versions DPA versions
	 */
	const versions: Dpa[] = [
		new Dpa('4.03', new OsDpaAttributes(false, true), 'https://repository.iqrfalliance.org/download/dpa/4.03'),
		new Dpa('4.10', new OsDpaAttributes(false, false), 'https://repository.iqrfalliance.org/download/dpa/4.10'),
	];

	beforeEach((): void => {
		mockedAxios.reset();
	});

	it('list all DPA versions', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/dpa')
			.replyOnce(200, rawVersions);
		const actual: Dpa[] = await service.list();
		expect(actual).toStrictEqual(versions);
	});

	it('list all obsolete DPA versions', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/dpa', { params: { obsolete: true } })
			.replyOnce(200, [rawVersions[0]]);
		const actual: Dpa[] = await service.list(new OsDpaAttributes(null, true));
		expect(actual).toStrictEqual([versions[0]]);
	});

	it('get DPA by version', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/dpa', { params: { version: '0410' } })
			.replyOnce(200, rawVersions[1]);
		const actual: Dpa = await service.get('4.10');
		expect(actual).toStrictEqual(versions[1]);
	});

	it('get DPA by version - invalid version', async (): Promise<void> => {
		expect.assertions(1);
		await expect(service.get('4.1')).rejects.toThrow('Invalid DPA version format');
	});

});
