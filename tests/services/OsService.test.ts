/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { beforeEach, describe, expect, it } from 'vitest';

import { OsService } from '../../src/services';
import { Os, OsDpaAttributes, type OsRaw, TrFamily } from '../../src/types';
import { mockedAxios, mockedClient } from '../mocks/axios';

describe('OsService', (): void => {

	/**
	 * @const {OsService} service IQRF OS service
	 */
	const service: OsService = new OsService(mockedClient);

	/**
	 * @const {OsRaw[]} rawVersions Raw response from the API
	 */
	const rawVersions: OsRaw[] = [
		{
			'build': '08D8',
			'version': '4.06D',
			'trFamily': '7xD',
			'attributes': 0,
			'downloadPath': 'https://repository.iqrfalliance.org/download/iqrfos/08D8',
		},
		{
			'build': '090F',
			'version': '4.06G',
			'trFamily': '7xG',
			'attributes': 0,
			'downloadPath': 'https://repository.iqrfalliance.org/download/iqrfos/090F',
		},
	];

	/**
	 * @const {Os[]} versions IQRF OS versions
	 */
	const versions: Os[] = [
		new Os('08D8', '4.06D', TrFamily.TR_7xD, new OsDpaAttributes(false, false), 'https://repository.iqrfalliance.org/download/iqrfos/08D8'),
		new Os('090F', '4.06G', TrFamily.TR_7xG, new OsDpaAttributes(false, false), 'https://repository.iqrfalliance.org/download/iqrfos/090F'),
	];

	beforeEach((): void => {
		mockedAxios.reset();
	});

	it('list all IQRF OS versions', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/os')
			.replyOnce(200, rawVersions);
		const actual: Os[] = await service.list();
		expect(actual).toStrictEqual(versions);
	});

	it('list all obsolete IQRF OS versions', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/os', { params: { obsolete: true } })
			.replyOnce(200, [rawVersions[0]]);
		const actual: Os[] = await service.list(new OsDpaAttributes(null, true));
		expect(actual).toStrictEqual([versions[0]]);
	});

	it('get IQRF OS by build', async (): Promise<void> => {
		expect.assertions(1);
		mockedAxios.onGet('/os', { params: { build: '090F' } })
			.replyOnce(200, rawVersions[1]);
		const actual: Os = await service.get('090F');
		expect(actual).toStrictEqual(versions[1]);
	});

	it('get IQRF OS by build - invalid build', async (): Promise<void> => {
		expect.assertions(1);
		await expect(service.get('0X0')).rejects.toThrow('Invalid IQRF OS build format');
	});

});
