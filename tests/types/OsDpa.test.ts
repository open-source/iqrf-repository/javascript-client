/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, expect, it } from 'vitest';

import { OsDpaAttributes } from '../../src/types';

describe('OsDpaAttributes', (): void => {

	it('stable and not obsolete attribute', (): void => {
		expect.assertions(8);
		const attributes: OsDpaAttributes = new OsDpaAttributes(null, null);
		expect(attributes.beta)
			.toBe(null);
		expect(attributes.obsolete)
			.toBe(null);
		expect(attributes.serialize())
			.toBe(0);
		expect(attributes.serializeMask())
			.toBe(0);
		expect(OsDpaAttributes.deserialize(0))
			.toEqual(new OsDpaAttributes(false, false));
		expect(attributes.getParams())
			.toEqual({});
		expect(attributes.getDpaParams())
			.toEqual({});
		expect(attributes.getOsParams())
			.toEqual({});
	});

	it('beta attribute', (): void => {
		expect.assertions(8);
		const attributes: OsDpaAttributes = new OsDpaAttributes(true, null);
		expect(attributes.beta)
			.toBeTruthy();
		expect(attributes.obsolete)
			.toBe(null);
		expect(attributes.serialize())
			.toBe(1);
		expect(attributes.serializeMask())
			.toBe(1);
		expect(OsDpaAttributes.deserialize(1))
			.toEqual(new OsDpaAttributes(true, false));
		expect(attributes.getParams())
			.toEqual({ beta: true });
		expect(attributes.getDpaParams())
			.toEqual({ dpaBeta: true });
		expect(attributes.getOsParams())
			.toEqual({ osBeta: true });
	});

	it('obsolete and stable attribute', (): void => {
		expect.assertions(8);
		const attributes: OsDpaAttributes = new OsDpaAttributes(false, true);
		expect(attributes.beta)
			.toBeFalsy();
		expect(attributes.obsolete)
			.toBeTruthy();
		expect(attributes.serialize())
			.toBe(2);
		expect(attributes.serializeMask())
			.toBe(3);
		expect(OsDpaAttributes.deserialize(2))
			.toEqual(new OsDpaAttributes(false, true));
		expect(attributes.getParams())
			.toEqual({ beta: false, obsolete: true });
		expect(attributes.getDpaParams())
			.toEqual({ dpaBeta: false, dpaObsolete: true });
		expect(attributes.getOsParams())
			.toEqual({ osBeta: false, osObsolete: true });
	});

});
