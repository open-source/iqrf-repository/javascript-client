/**
 * Copyright 2023-2025 MICRORISC s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, expect, it } from 'vitest';

import {
	AccumulatorPower,
	BatteryPower,
	HwpidVersions,
	MetadataProfile,
	PowerSupply,
	ProductMetadata,
	StaticMetadata,
} from '../../src/types';

describe('ProductMetadata', (): void => {

	const metadata: ProductMetadata = new ProductMetadata(
		[new StaticMetadata(
			0,
			[
				new MetadataProfile(
					new HwpidVersions(0, 2),
					true,
					false,
					false,
					false,
					true,
					[1, 129, 129],
					4,
					new PowerSupply(
						false,
						new AccumulatorPower(true, 'LIP552240', 3_400),
						new BatteryPower(false, null, null),
						2_200,
					),
				),
				new MetadataProfile(
					new HwpidVersions(3, -1),
					true,
					false,
					false,
					false,
					true,
					[1, 129, 129],
					4,
					new PowerSupply(
						false,
						new AccumulatorPower(true, 'LIP552240', 3_400),
						new BatteryPower(false, null, null),
						2_200,
					),
				),
			],
		)],
		[
			{
				'version': 0,
				'productionTest': [],
			},
		],
	);

	it('get without metadata or hwpid version', (): void => {
		expect.assertions(1);
		expect(metadata.getProfile())
			.toEqual(metadata.static[0].profiles[1]);
	});

	it('get with valid metadata version and without hwpid version', (): void => {
		expect.assertions(1);
		expect(metadata.getProfile(null, 0))
			.toEqual(metadata.static[0].profiles[1]);
	});

	it('get without metadata version and with hwpid version', (): void => {
		expect.assertions(1);
		expect(metadata.getProfile(2))
			.toEqual(metadata.static[0].profiles[0]);
	});

	it('get with valid metadata and hwpid version', (): void => {
		expect.assertions(1);
		expect(metadata.getProfile(0, 0))
			.toEqual(metadata.static[0].profiles[0]);
	});

	it('get with any metadata version and valid hwpid version', (): void => {
		expect.assertions(1);
		expect(metadata.getProfile(1))
			.toEqual(metadata.static[0].profiles[0]);
	});

	it('get with invalid metadata version and valid hwpid version', (): void => {
		expect.assertions(1);
		expect(metadata.getProfile(0, 1))
			.toBeNull();
	});

	it('get with valid metadata version and invalid hwpid version', (): void => {
		expect.assertions(1);
		expect(metadata.getProfile(-2, 0))
			.toBeNull();
	});

});
