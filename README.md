# IQRF Repository JavaScript client

[![Build Status](https://gitlab.iqrf.org/open-source/iqrf-repository/javascript-client/badges/master/pipeline.svg)](https://gitlab.iqrf.org/open-source/iqrf-repository/javascript-client/-/commits/master)
[![Test coverage](https://gitlab.iqrf.org/open-source/iqrf-repository/javascript-client/badges/master/coverage.svg)](https://gitlab.iqrf.org/open-source/iqrf-repository/javascript-client/-/commits/master)
[![NPM Version](https://img.shields.io/npm/v/%40iqrf%2Fiqrf-repository-client)](https://www.npmjs.com/package/@iqrf/iqrf-repository-client)
[![NPM Downloads](https://img.shields.io/npm/dm/%40iqrf%2Fiqrf-repository-client)](https://www.npmjs.com/package/@iqrf/iqrf-repository-client)
[![Apache License](https://img.shields.io/badge/license-APACHE2-blue.svg)](LICENSE)
[![API documentation](https://img.shields.io/badge/docs-api-brightgreen.svg)](https://apidocs.iqrf.org/js/iqrf-repository-client/)

## Installation

```bash
npm install @iqrf/iqrf-repository-client
```

## How to use

Client is a wrapper around API methods of IQRF Repository providing shared configuration for Axios instance.

### Instantiate with defaults
```typescript
import { Client } from '@iqrf/iqrf-repository-client';

const client = new Client();
```

### Instantiate with custom configuration
```typescript
import { Client } from '@iqrf/iqrf-repository-client';

const config: AxiosRequestConfig = {
	baseURL: 'https://devrepo.iqrfalliance.org/api/',
};
const client = new Client({ config });
```

### Instantiate with custom Axios instance **advanced**
```typescript
import axios, { type AxiosRequestConfig } from 'axios';

import { Client } from '@iqrf/iqrf-repository-client';

const config: AxiosRequestConfig = {
	baseURL: 'https://repository.iqrfalliance.org/api/',
};
const axiosInstance = axios.create(config);
const client = new Client({ axiosInstance });
```
